# minikube tekton gitlab eventlisteners test

## requirements

* minikube
* helm
* kubectl
* terraform
* docker

## run

```bash
make full
```

## test

* check `kubectl get taskruns.tekton.dev`
