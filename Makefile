INIT_TOKEN = "suuuper-s3cret-token"
GITLAB_REPO = "gitlab-config"
MINIKUBE_IP = $(shell minikube ip)
YQ_COMMAND = docker run --rm -i -v "$${PWD}":/workdir mikefarah/yq
KUBECTL_TIMEOUT = 480s

start-mini:
	minikube start --cpus 4 --memory 8g --addons ingress

rm:
	minikube delete

gitlab-repo:
	helm repo add gitlab https://charts.gitlab.io/
	helm repo update

gitlab:
	helm upgrade --install gitlab gitlab/gitlab -f values-gitlab.yml

tekton:
	kubectl apply --filename https://storage.googleapis.com/tekton-releases/pipeline/latest/release.yaml
	kubectl apply --filename https://storage.googleapis.com/tekton-releases/triggers/latest/release.yaml
	kubectl apply --filename https://storage.googleapis.com/tekton-releases/triggers/latest/interceptors.yaml
	kubectl apply --filename https://storage.googleapis.com/tekton-releases/dashboard/latest/tekton-dashboard-release.yaml
	kubectl apply --filename ./dashboard/ingress.yaml

.PHONY: el
el:
	kubectl apply --filename el/

initial-token:
	kubectl exec -it $(shell kubectl get pods -lapp=toolbox -oname) -- /srv/gitlab/bin/rails runner "token = User.find_by_username('root').personal_access_tokens.create(scopes: [:api], name: 'Automation token'); token.set_token('$(INIT_TOKEN)'); token.save!"

configure-gitlab:
	terraform -chdir=$(GITLAB_REPO) init
	terraform -chdir=$(GITLAB_REPO) apply  -var gitlab_token=$(INIT_TOKEN) -var minikubeip=$(MINIKUBE_IP) -auto-approve

wait-for-gitlab:
	kubectl wait --timeout=$(KUBECTL_TIMEOUT) --for=condition=ready pod -l app=webservice

fix-minikube-ip:
	docker pull mikefarah/yq
	$(YQ_COMMAND) -i '.global.hosts.domain = "$(MINIKUBE_IP).nip.io"' values-gitlab.yml
	$(YQ_COMMAND) -i '.global.hosts.externalIP = "$(MINIKUBE_IP)"' values-gitlab.yml
	$(YQ_COMMAND) -i '.spec.rules[0].host = "trigger.$(MINIKUBE_IP).nip.io"' el/ingress.yaml
	$(YQ_COMMAND) -i '.spec.rules[0].host = "tkn-dashboard.$(MINIKUBE_IP).nip.io"' dashboard/ingress.yaml

get-login-info:
	@echo "url: $(shell kubectl get ingress gitlab-webservice-default -ojsonpath='{.spec.rules[0].host}')\nuser: root\npass: $(shell kubectl get secret gitlab-gitlab-initial-root-password -ojsonpath='{.data.password}' | base64 --decode)"

full: start-mini fix-minikube-ip gitlab-repo gitlab tekton wait-for-gitlab el initial-token configure-gitlab get-login-info
